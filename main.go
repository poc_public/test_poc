package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		ss := `<!doctype html>
			<html><head>
			<meta charset="utf-8">
			</head>
			<body>
			</body>
				<center>
				<h1>欢迎您的到来</h1>
				</center>
			</html>
			`
		w.Write([]byte(ss))
	}) //   设置访问路由
	http.HandleFunc("/ping/ok", func(w http.ResponseWriter, r *http.Request) {

		bs, _ := json.Marshal(map[string]interface{}{
			"status": "ok",
		})
		w.Write(bs)
	}) //   设置访问路由
	fmt.Println("启动服务，访问端口8000")
	http.ListenAndServe(":8000", nil) //设置监听的端口
}
