###################################
#Build stage
FROM golang:1.19-alpine AS build

#创建目录,并copy代码
WORKDIR /test_poc/
COPY ./ /test_poc

# 编译
RUN go build -mod=vendor -o /test_poc/bin/server

# 运行镜像
FROM alpine
ENV TZ Asia/Shanghai
COPY --from=build /test_poc/bin/ /work/

# 定义工作目录为work
WORKDIR /work

# 开放 服务端口
EXPOSE 8000
# 启动http服务
ENTRYPOINT ["./server"]